# AbInBev CDP integration.

This module provides integration feature for integration webform to
CDP database (https://treasuredata.com).

It provides Webform Handler for data processing. 

Install as usual drupal module.

# Module Settings.

- Add Handler to your webform.
- Configure Handler - make mapping of webform fields to CDP fields.
- For additional validation - please create new custom handler 
- and put it before this and write custom validation there.
- Mapping of additional fields is in 'additional fields' field.
- Please use format cdp_field_machine_name|webform_field_machine_name

## Setting sections

### User Settings
This section contains settings for fields that are required by CDP.
First name, last name, email or phone.

Birthday is not required, but if it is enabled additional validation 
will be enabled.
