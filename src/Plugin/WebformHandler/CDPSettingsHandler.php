<?php

namespace Drupal\abinbev_cdp\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission settings handler.
 *
 * @WebformHandler(
 *   id = "cdp_settings_handler",
 *   label = @Translation("CDP Settings Handler"),
 *   category = @Translation("CDP Settings"),
 *   description = @Translation("Allows CDP Webform Handler to be used by
 *   differenе webform with dynamic settings."), cardinality =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission =
 *   \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class CDPSettingsHandler extends WebformHandlerBase {

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->typedConfigManager = $container->get('config.typed');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Preview settings.
    $form['user_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('User settings'),
      '#open' => TRUE,
    ];
    $form['user_settings']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform Name Field'),
      '#description' => $this->t('Machine name of field for Name.'),
      '#default_value' => $this->configuration['user_settings']['name'] ?? 'first_name',
    ];
    $form['user_settings']['surname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform Surname Field'),
      '#description' => $this->t('Machine name of field for Surname.'),
      '#default_value' => $this->configuration['user_settings']['surname'] ?? 'last_name',
    ];
    $form['user_settings']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform Email Field'),
      '#description' => $this->t('Machine name of field for Email.'),
      '#default_value' => $this->configuration['user_settings']['email'] ?? 'email',
    ];

    $form['user_settings']['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform Phone Field'),
      '#description' => $this->t('Machine name of field for Phone.'),
      '#default_value' => $this->configuration['user_settings']['phone'] ?? 'phone',
    ];

    $form['user_settings']['birthday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform birthday Field'),
      '#description' => $this->t('Machine name of field for birthday.'),
      '#default_value' => $this->configuration['user_settings']['birthday'] ?? 'birthday',
    ];

    $form['user_settings']['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webform Language'),
      '#description' => $this->t('Language code. ex. en, fr, it'),
      '#default_value' => $this->configuration['user_settings']['language'] ?? 'language',
    ];

    $form['user_settings']['additional_fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional fields.'),
      '#description' => $this->t('Add additional fields to CDP request. Use format cdp_field|webform_field. One field per row.'),
      '#default_value' => $this->configuration['user_settings']['additional_fields'] ?? '',
    ];

    $form['purposes'] = [
      '#type' => 'details',
      '#title' => $this->t('Purposes settings'),
      '#open' => TRUE,
    ];

    $form['purposes']['tcpp'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TCPP'),
      '#description' => $this->t('tcpp'),
      '#default_value' => $this->configuration['purposes']['tcpp'] ?? 'tcpp',
    ];

    $form['purposes']['marketing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Marketing'),
      '#description' => $this->t('marketing'),
      '#default_value' => $this->configuration['purposes']['marketing'] ?? 'marketing',
    ];

    $form['purposes']['newsletter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Newsletter'),
      '#description' => $this->t('Newsletter'),
      '#default_value' => $this->configuration['purposes']['newsletter'] ?? 'newsletter',
    ];

    $form['campaign_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Campaign Details'),
      '#open' => TRUE,
    ];
    $form['campaign_details']['campaign_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Campaign Name'),
      '#description' => $this->t('Campaign name'),
      '#default_value' => $this->configuration['campaign_details']['campaign_name'] ?? '',
    ];
    $form['campaign_details']['form_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Id'),
      '#description' => $this->t('Form Id'),
      '#default_value' => $this->configuration['campaign_details']['form_id'] ?? '',
    ];
    $form['campaign_details']['brand'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Brand'),
      '#description' => $this->t('Brand name'),
      '#default_value' => $this->configuration['campaign_details']['brand'] ?? '',
    ];

    $form['campaign_details']['interests'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Interests'),
      '#description' => $this->t('Interests'),
      '#default_value' => $this->configuration['campaign_details']['interests'] ?? '',
    ];

    $form['campaign_details']['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country'),
      '#options' => $this->getCountryList(),
      '#description' => $this->t('Select Country'),
      '#default_value' => $this->configuration['campaign_details']['country'] ?? 'bel',
    ];

    $form['campaign_details']['dynamical_country'] = [
      '#type' => 'boolean',
      '#title' => $this->t('Country based on language'),
      '#description' => $this->t('Country based on language. Based on current language of page.'),
      '#default_value' => $this->configuration['campaign_details']['dynamical_country'] ?? FALSE,
    ];

    $form['campaign_details']['is_production'] = [
      '#type' => 'radios',
      '#title' => $this->t('Is Production'),
      '#options' => [
        'dev' => $this->t('DEV'),
        'prod' => $this->t('PROD'),
      ],
      '#description' => $this->t('Dev/Prod state'),
      '#default_value' => $this->configuration['campaign_details']['is_production'] ?? 'dev',
    ];

    $form['campaign_details']['cdp_key_dev'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDP API DEV key'),
      '#description' => $this->t('CDP API Dev key'),
      '#default_value' => $this->configuration['campaign_details']['cdp_key_dev'] ?? '',
    ];

    $form['campaign_details']['cdp_key_prod'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDP API PROD key'),
      '#description' => $this->t('CDP API Prod key'),
      '#default_value' => $this->configuration['campaign_details']['cdp_key_prod'] ?? '',
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'user_settings' => [],
      'purposes' => [],
      'campaign_details' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Completely reset configuration so that custom configuration will always
    // be reset.
    $this->configuration = $this->defaultConfiguration();

    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);

    // Remove all empty strings from preview and confirmation settings.
    $this->configuration = array_filter($this->configuration);

    // Append custom settings to configuration.
    $this->configuration += $form_state->getValues();

  }

  /**
   * Apply submitted form state to configuration.
   *
   * This method can used to update configuration when the configuration form
   * is being rebuilt during an #ajax callback.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function applyFormStateToConfiguration(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $default_configuration = $this->defaultConfiguration();

    foreach ($values as $key => $value) {
      if (array_key_exists($key, $this->configuration)) {
        if (is_bool($default_configuration[$key])) {
          $this->configuration[$key] = (boolean) $value;
        }
        elseif (is_int($default_configuration[$key])) {
          $this->configuration[$key] = (integer) $value;
        }
        else {
          $this->configuration[$key] = $value;
        }
      }
    }
  }

  /**
   * Get webform setting definitions.
   *
   * @return array
   *   Webform setting definitions defined in webform.entity.webform.schema.yml
   */
  protected function getSettingsDefinitions() {
    $definition = $this->typedConfigManager->getDefinition('webform.webform.*');
    return $definition['mapping']['settings']['mapping'];
  }

  /**
   * Get webform submission's overridden settings.
   *
   * Replaces submissions token values and cast booleans and integers.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   *
   * @return array
   *   An associative array containing overridden settings.
   */
  protected function getSubmissionSettingsOverride(WebformSubmissionInterface $webform_submission) {
    $settings_definitions = $this->getSettingsDefinitions();
    $settings_override = $this->getSettingsOverride();
    foreach ($settings_override as $name => $value) {
      if (!isset($settings_definitions[$name])) {
        continue;
      }

      // Replace token value and cast booleans and integers.
      $type = $settings_definitions[$name]['type'];
      if (in_array($type, ['boolean', 'integer'])) {
        $value = $this->replaceTokens($value, $webform_submission);
        settype($value, $type);
        $settings_override[$name] = $value;
      }
    }
    return $settings_override;
  }

  /**
   * Get country list.
   *
   * @return string[]
   *   An String array will return list of countries.
   */
  public function getCountryList() {
    $country_list = [
      "nga" => "Nigeria",
      "zwe" => "Zimbabwe",
      "zaf" => "South Africa",
      "aus" => "Australia",
      "chn" => "China",
      "ind" => "India",
      "jpn" => "Japan",
      "kor" => "Korea",
      "tha" => "Thailand",
      "vnm" => "Vietnam",
      "bel" => "Belgium",
      "fra" => "France",
      "deu" => "Germany",
      "ita" => "Italy",
      "nld" => "Netherlands",
      "rus" => "Russia",
      "esp" => "Spain",
      "ukr" => "Ukraine",
      "gbr" => "Great Britain",
      "col" => "Colombia",
      "dom" => "Dominican Republic",
      "ecu" => "Ecuador",
      "slv" => "El Salvador",
      "gtm" => "guatemala",
      "hnd" => "Honduras",
      "mex" => "Mexico",
      "pan" => "Panama",
      "per" => "Peru",
      "can" => "Canada",
      "usa" => "United States",
      "arg" => "Argentina",
      "bol" => "Bolivia",
      "bra" => "Brazil",
      "chl" => "Chili",
      "ury" => "Uruguay",
    ];
    return $country_list;
  }

  /**
   * Get overridden settings.
   *
   * @return array
   *   An associative array containing overridden settings.
   */
  protected function getSettingsOverride() {
    $settings = $this->configuration;
    unset($settings['debug']);
    $default_configuration = $this->defaultConfiguration();
    foreach ($settings as $name => $value) {
      if (isset($default_configuration[$name]) && $default_configuration[$name] === $value) {
        unset($settings[$name]);
      }
    }
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function overrideSettings(array &$settings, WebformSubmissionInterface $webform_submission) {
    $settings_override = $this->getSubmissionSettingsOverride($webform_submission);
    foreach ($settings_override as $name => $value) {
      $settings[$name] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $user_settings = $this->configuration['user_settings'];
    $purposes_settings = $this->configuration['purposes'];
    $campaign_details = $this->configuration['campaign_details'];
    $values = $webform_submission->getData();

    $language = $values[$user_settings['language']];

    if (empty($language)) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
    }
    if (strpos($language, '-')) {
      $prefixes = explode('-', $language);
      if (!empty($prefixes[1])) {
        $language = $prefixes[1];
      }
    }

    // Define variable that will be used to tell the __sendTD method
    // if it should send to the production database.
    $is_production = $campaign_details['is_production'] == 'prod' ?? FALSE;

    // Define the purpose variable as an empty array.
    $purposes = [];

    // Check whether the TC-PP checkbox is checked, and if it is,
    // then add it to the purpose array - informed.
    if ($values[$purposes_settings['tcpp']]) {
      $purposes[] = 'TC-PP';
    }

    // Check whether the MARKETING-ACTIVATION checkbox is checked,
    // and if it is, then adds it to the purpose array.
    $purposes[] = !empty($purposes_settings['marketing']) && !empty($values[$purposes_settings['marketing']]) ? 'MARKETING-ACTIVATION' : 'MARKETING-ACTIVATION-NOT-GIVEN';

    // Newsletter checkbox.
    $purposes[] = (!empty($purposes_settings['newsletter']) && !empty($values[$purposes_settings['newsletter']])) ? 'PERSONALIZATION' : 'PERSONALIZATION-NOT-GIVEN';

    $country = $campaign_details['country'] ?? 'bel';

    $additional_fields = [];
    if (!empty($user_settings['additional_fields'])) {

      $rows = explode(PHP_EOL, trim($user_settings['additional_fields']));
      if (!empty($rows)) {
        foreach ($rows as $row) {
          $fields = explode('|', trim($row));
          if (!empty($fields)) {
            if (isset($values[$fields[1]])) {
              $additional_fields[$fields[0]] = $values[$fields[1]];
            }
          }
        }
      }
    }

    $td_values = [];

    if (!empty($user_settings['name']) && !empty($values[$user_settings['name']])) {
      $td_values['abi_firstname'] = $values[$user_settings['name']];
    }

    if (!empty($user_settings['surname']) && !empty($values[$user_settings['surname']])) {
      $td_values['abi_lastname'] = $values[$user_settings['surname']];
    }

    if (!empty($user_settings['email']) && !empty($values[$user_settings['email']])) {
      $td_values['abi_email'] = $values[$user_settings['email']];
    }

    if (!empty($user_settings['phone']) && !empty($values[$user_settings['phone']])) {
      $td_values['abi_phone'] = $values[$user_settings['phone']];
    }

    if (!empty($user_settings['birthday']) && !empty($values[$user_settings['birthday']])) {
      $td_values['abi_birthday'] = $values[$user_settings['birthday']];
    }

    $td_values['abi_country'] = $country;
    $td_values['abi_preferred_language'] = $language;
    $td_values['purpose_name'] = $purposes;
    $td_values['abi_interests'] = !empty($campaign_details['interests']) ? $campaign_details['interests'] : NULL;
    $td_values += $additional_fields;

    // Runs the __sendTD method with parameters got from the request,
    // it should be changed based on your form fields, country, brand, campaign,
    // form, and whether if it's running in the production environment or not.
    $this->__sendTD(
      $td_values,
      $country,
      $campaign_details['brand'],
      $campaign_details['campaign_name'],
      $campaign_details['form_id'],
      TRUE,
      $is_production,
      $campaign_details
    );
  }

  /**
   * Custom webform validate handler.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webformsubmission to get the data of subimmited webforms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $user_settings = $this->configuration['user_settings'];
    $birthday = $user_settings['birthday'];
    $values = $webform_submission->getData();
    if (empty($birthday) || empty($values[$birthday])) {
      return;
    }
    $date1 = new \DateTime();
    $date1 = $date1::createFromFormat('Y-m-d', $values[$birthday]);
    $date2 = new \DateTime();
    $interval = $date1->diff($date2);

    if ($interval->y < 18) {
      $form_state->setErrorByName($values[$birthday], $this->t('You must to be  18 years old at least.'));
    }

  }

  /**
   * Send to CDP.
   *
   * @param string $form_data
   *   The Form Data.
   * @param string $country
   *   The Country.
   * @param string $brand
   *   The brand.
   * @param string $campaign
   *   The campaign.
   * @param string $form
   *   The Form.
   * @param string $unify
   *   The unify.
   * @param string $production
   *   The production.
   * @param string $campaign_details
   *   The campaign details.
   *
   * @return mixed
   */
  private function __sendTD($form_data, $country, $brand, $campaign, $form, $unify, $production, $campaign_details) {
    $http_protocol = isset($_SERVER['https']) ? 'https://' : 'http://';

    $form_data['abi_brand'] = $brand;
    $form_data['abi_campaign'] = $campaign;
    $form_data['abi_form'] = $form;
    $form_data['td_unify'] = $unify;
    $form_data['td_import_method'] = 'postback-api-1.2';
    $form_data['td_client_id'] = $_COOKIE['_td'] ?? '';
    $form_data['td_url'] = $http_protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $form_data['td_host'] = $_SERVER['HTTP_HOST'];

    $td_country = $country;

    $td_apikey = $campaign_details['is_production'] == 'dev' ? $campaign_details['cdp_key_dev'] : $campaign_details['cdp_key_prod'];

    $country_zone_mapping = [
      "nga" => "africa",
      "zwe" => "africa",
      "zaf" => "africa",
      "aus" => "apac",
      "chn" => "apac",
      "ind" => "apac",
      "jpn" => "apac",
      "kor" => "apac",
      "tha" => "apac",
      "vnm" => "apac",
      "bel" => "eur",
      "fra" => "eur",
      "deu" => "eur",
      "ita" => "eur",
      "nld" => "eur",
      "rus" => "eur",
      "esp" => "eur",
      "ukr" => "eur",
      "gbr" => "eur",
      "col" => "midam",
      "dom" => "midam",
      "ecu" => "midam",
      "slv" => "midam",
      "gtm" => "midam",
      "hnd" => "midam",
      "mex" => "midam",
      "pan" => "midam",
      "per" => "midam",
      "can" => "naz",
      "usa" => "naz",
      "arg" => "saz",
      "bol" => "saz",
      "bra" => "saz",
      "chl" => "saz",
      "ury" => "saz",
    ];

    $td_zone = $country_zone_mapping[$td_country];
    $curl = curl_init();
    $curl_opts = [
      CURLOPT_URL => "https://in.treasuredata.com/postback/v3/event/{$td_zone}_source/{$td_country}_web_form",
      CURLOPT_POST => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/json',
        "X-TD-Write-Key: {$td_apikey}",
      ],
      CURLOPT_POSTFIELDS => json_encode($form_data),
    ];

    curl_setopt_array($curl, $curl_opts);

    $response = @curl_exec($curl);
    $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    return $response_code;
  }

}
